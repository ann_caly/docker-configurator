<?php

// ------- uploads.json -------

$uploads_arr = array();
$uploadsini = array(short_open_tag, upload_max_filesize, max_file_uploads, memory_limit, post_max_size, allow_url_fopen);
foreach ($uploadsini as $val) { 
	$upload = ini_get($val);
	array_push($uploads_arr, $upload);
}
$uploads = array_combine($uploadsini, $uploads_arr);

$fp = fopen('data.json', 'w');
fwrite($fp, json_encode($uploads));
fclose($fp);

// ------- Dockerfile -------

ob_start();
include($_SERVER['DOCUMENT_ROOT'].'/generator/templates/Dockerfile.tpl');
$dockerfile = ob_get_contents();
ob_end_clean();

if (!is_dir('results')) {
  mkdir('results', 0777, true);
}
chdir('results');
$fp = fopen('Dockerfile', 'w');
fwrite($fp, $dockerfile);
fclose($fp);

// ------- docker-compose.yaml -------

ob_start();
include($_SERVER['DOCUMENT_ROOT'].'/generator/templates/docker-compose.tpl');
$dockercompose = ob_get_contents();
ob_end_clean();

chdir('results');
$fp = fopen('docker-compose.yaml', 'w');
fwrite($fp, $dockercompose);
fclose($fp);

// ------- uploads.ini -------

ob_start();
include($_SERVER['DOCUMENT_ROOT'].'/generator/templates/uploads.tpl');
$uploadsini = ob_get_contents();
ob_end_clean();

chdir('results');
$fp = fopen('uploads.ini', 'w');
fwrite($fp, $uploadsini);
fclose($fp);

// ------- archive -------

if(isset($_POST['create'])) {
	$pathdir= $_SERVER['DOCUMENT_ROOT'] . '/generator/results';
	$zipname = 'results.zip';
	$files = array_slice(scandir($pathdir), 2);
	$zip = new ZipArchive;

	if ($zip->open($zipname, ZipArchive::CREATE) === TRUE){
	  foreach ($files as $file) {
			$zip->addFile($file);
		}
	$zip->close();

	// ------- download -------
	header('Content-Type: application/zip');
	header('Content-disposition: attachment; filename='.$zipname);
	header('Content-Length: ' . filesize($zipname));
	readfile($zipname);
	} else {
	  die ('Произошла ошибка при создании архива');
	}
}

?>