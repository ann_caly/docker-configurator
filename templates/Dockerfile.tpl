FROM php:<?php echo $php_image; ?>-apache

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        zlib1g-dev \
        <?php if (strpos($extensions, 'xml') !== false) {
                echo "libxml2-dev \ \n";
            }
              if (strpos($extensions, 'curl') !== false) {
                echo "libcurl4-gnutls-dev \ \n";
            }
        ?>
    && pecl install xdebug \
    && docker-php-ext-install mbstring zip <?php echo $extensions; ?> \
    <?php if (strpos($extensions, 'gd') !== false) {
    	echo "&& docker-php-ext-configure gd --with-jpeg-dir=/usr/include/ \ \n";
		} 
		?>
		&& docker-php-ext-enable xdebug
    
 
RUN a2enmod  rewrite 

# CMD ["apache2-foreground"]
