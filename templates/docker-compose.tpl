version: '3'
services:
  db:
    image: mysql:<?php echo $mysqli_image . "\n"; ?>
    volumes:
     - ../db_data:/var/lib/mysql
     - ../db_init:/docker-entrypoint-initdb.d
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: dobro
      MYSQL_DATABASE: cosmed
      MYSQL_PASSWORD: dobro
  web:
    depends_on:
      - db
    build: .
    volumes:
      - ../app:/var/www/html
      - ../uploads.ini:/usr/local/etc/php/conf.d/uploads.ini
    ports:
      - "8000:80"
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_NAME: cosmed
      WORDPRESS_DB_USER: root
      WORDPRESS_DB_PASSWORD: dobro
  phpmyadmin:
    depends_on:
      - db
    image: phpmyadmin/phpmyadmin
    restart: always
    ports:
      - 8080:80
    environment:
      PMA_HOST: db
      MYSQL_ROOT_PASSWORD: dobro