<?php

include ('env.php');
include ('generator.php');
//include ('dumper.php');

echo "
<!DOCTYPE html>
<html lang='ru'>
  <head>
    <meta charset='utf-8' />
      <title>Docker-конфигуратор</title>
      <link rel='stylesheet' type='text/css' href='style.css'>
  </head>
  <body>
    <main>
    	<section class='versions'>";
		    echo "<span class='bold'>Версия PHP:</span>" . " " . phpversion() . '<br />';
		    echo "<span class='bold'>Версия MySQL:</span>" . " " . $mysqli;
		    echo "</section>
		  <section class='extensions'>
	    	<p class='title'>Список используемых расширений PHP</p>
	    		<div class='ext_list'>"; 
					echo '<ul>';
					  foreach ($exts as $key => $val) {  
						  echo '<li>' . $val . '</li>';
						}
					echo "</ul>
	      </div>
      </section>
			<section class='extensions'>
			  <p class='title'>Параметры php.ini</p>";
					foreach ($uploads as $key => $val) {  
						echo '<div>' . $key . ' ' . '=' . ' ' . $val . '</div>';
					}
			echo "</section>
			<section class='download'>
			  <p class='title'>Скачать архив файлов конфигурации</p>
			  <form method='post' action=''>
			    <input type='submit' name='create' value='Загрузить' />&nbsp;
			  </form>
			</section>
		</main>
	</body>
</html>";

?>