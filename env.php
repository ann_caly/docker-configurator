<?php

preg_match("#^\d+(\.\d+)*#", phpversion(), $match);
$ver = $match[0];
$php_image = substr($ver, 0, 3);

// ------- MySQL --------

$link = mysqli_connect("localhost", "root", "dobro");

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$mysqli = mysqli_get_server_info($link);
$mysqli_image = substr($mysqli, 0, 3);

mysqli_close($link);

// -------- extensions --------

$arr = array();
$exts = array();
$mod = array(xml, gd, mbyte, mcrypt, curl, mysqli);
foreach (get_loaded_extensions() as $i => $ext) {
	$ext_array = array_push($exts, $ext);
	foreach ($mod as $key => $val) {  
	  if($ext == $val) {
	  	array_push($arr, $ext);
	  }
	}
}
$extensions = implode(" ", $arr);

?>

